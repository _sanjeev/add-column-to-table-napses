'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    return Promise.all ([
      queryInterface.addColumn(
        'users',
        'twitter', 
        {
          type: Sequelize.STRING,
          allowNull: true,
          defaultValue: "Joined"
        },
      ),
      queryInterface.addColumn(
        'users',
        'linkedin',
        {
          type: Sequelize.STRING,
          allowNull: true,
        },
      ),
      queryInterface.addColumn(
        'users',
        'bio',
        {
          type: Sequelize.TEXT,
          allowNull: true,
        },
      ),])
  },

  async down (queryInterface, Sequelize) {
     return Promise.all([
      queryInterface.removeColumn('users', 'linkedin'),
      queryInterface.removeColumn('users', 'twitter'),
      queryInterface.removeColumn('users', 'bio'),
    ]);
  }
};
