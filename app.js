const {sequelize, User} = require('./models');
const express = require ('express');
const res = require('express/lib/response');

const app = express();
app.use (express.json());

app.post ('/users', async (req, res) => {
    const obj = req.body;
    try {
        const users = await User.create (obj);
        return res.json(users);
    } catch (error) {
        console.log(error);
        return res.json(error);
    }
})

app.listen({port: 5000}, async() => {
    console.log('Server hosted on localhost:5000');
    await sequelize.sync();
    console.log("Database Connected");
})